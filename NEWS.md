## September 9, 2024 (1.0.0.9005, 9006)
* Fix bug that prevented looking up projects by row number. 
* Fix new bug in `process.tasks()`

## September 9, 2024 (1.0.0.9004)

* Add kludge to work around the inability to launch processes from R that 
persist after the launching thread exits.  
  * New config option `r_can_launch` controls behavior.  If set to `0` 
  (r cannot launch) than anytime R would launch APL or refresh it will
  instead retire, leaving a missing thread for APL maintenance threads to
  relaunch.  In theory we could also launch from R and have persistent R 
  maintenance threads that launch everything else and handle relaunching.  
  *   `process_tasks()` checks to see if `r_can_launch` is 0 and if so
  and it was going to refresh or switch it instead retires.
  * `launch_more()` now checks `r_can_launch`  before launching.
  * Note: given the issues with launching fully testing the switch to a target 
  number of threads for R was difficult. If at some future time the launching
  issue is resolved R's ability to relaunch threads should be tested.

* add `ant.message()` function to simplify printing messages only under certain
  verbosity settings.

## February 29, 2024 (1.0.0.9003)

* Switch to target threads for each computer.
  * Add `computers$target`
  * Drop `computers$killed`
  * Update `launch()`, `launch.more()`, `kill()`, `start.thread()` so that
  threads running anthill work together to maintain the target number of 
  threads on each computer. 

## February 25, 2024 (1.0.0.9003)

* Convert to roxygen2 based documentation
* Rename `tm.time.format()` to `anthill.time.format()`
* Make `anthill.time.format()` public
* Fix some check issues
* I think a little of the switch to target threads made it in here too.

## February 22, 2022 (1.0.0.9001)
  * Added 64 bit dll
  * Updated 32 bit dll
  * Adopted semantic versioning
  * Added NEWS.md (contents previously were in readme.txt)
  * Added README.md
  * Dropped readme.txt
  * Added .gitattributes file
  * Fixed many (not all) issues raised by package check
  * Spell check

## June 6, 2022 (0.113 - 0.115)
  
* Added minimal support for new `maxpernode` task column which will allow
  users to limit the number of nodes that a task will run on each computer. 
  This allows limiting and distributing memory intensive tasks so that 
  we don't blow out memory on any one machine. Currently I've added code
  to read and write the new column and for launch.project (and thus
  also simple.launch) to add the new column with a value of zero.
  
* New function `set.maxpernode()` allows changing this value after launching.
  
* Note: if there are more than config.txt -> maxsubtasks subtasks than 
  the task may be split resulting in this parameter applying to each 
  resulting task and thus subverting its purpose. I haven't attempted
  to code around this but suggest if you need this functionality that
  you turn off task splitting or set that parameter sufficiently 
  high not to invoke it.
 
## March 30, 2022 (0.112)

* refresh has new argument "what" that allows you to refresh target
  threads or computers as well as entire cluster.

## March 29, 2022 (0.111)

* Cleaned up documentation.

## March 23, 2022 (0.110) 
  
  * Added functions to access details about anthill and running sessions:
     * get.taskdetails()  returns information about current task including
       it's directory, workspace, task id, and subtask id.
     * get.ant.path() returns the path to the anthill base directory.  It's
       called by gridio in several places (e.g. gridio::launchconversion())
     * An older, similar function get.project() wasn't exported and now is. 
  
  * get.pid now uses gridio2 preferentially over gridio and doesn't attach 
      either package (as it previously did).
      
  * New function `set.owner()` sets a global value `.settings$owner` which is 
    now the default owner for `launch.project()` and indirectly 
    `simple.launch()`.  
    I haven't systematically looked for other places where owner is used but
    intend to eventually switch the default to this global setting.  
    Launching is the big one though!
    

## March 18, 2022 (0.110)

 * Exports anthill settings to gridio2 as well as gridio on config() (supporting
  transition to tiffs in gridio2)

## December, 7, 2021 (0.109) 

*  Changed settings to support changes in gridio,
  specifically added `tiff` (for geoTIFFs vs ESRI grids) and replaced referencegrid 
  and refrencemosaic with reference (the base reference path - amended by 
  adding ".tif", "_tm", or "_m" as needed for TIFF reference grids, TIFF based 
  mosaics, and grid based mosaics.  As is it should be the reference ESRI
  grid.  It's not necessary that all four forms exist on disk.
*  Added call to gridio::importantconfig() to config() after checking if 
  gridio is loaded. This will mean that gridio is configured based on 
  anthill regardless of load order and that changes to config.txt reloaded
  into anthill will propagate to gridio immediately.

## April 22, 2021 (0.108) 

* Added `arcgishost`, `referencegrid`, and `referencemosaic` to 
  config file and it's default settings in setting.R. gridio will now
  use these when launching`batchstitch()` and `batchmosaic()`

## March  22, 2021 (0.107)

* Added `gridserverpostfix` to settings with default value
  and description and edited `launch.gridservers()` and `relaunch.gridservers()`
  where the postfix is appended  to  the computer name before the computer name
  is compared to `gridservers.txt`.  This means we can list the computer names
  as DSL00-f in `gridervers.txt` and it will register as the same computer
  as DSL00    the "-f"  is used to indicate the back network on our cluster
  which is 10 times faster than the front network.

## March  2, 2021 (0.106)
  
* Changed default path from z:/anthill to x:/anthill also
  when anthill is invoked without a path it now checks for an Anthill_Path
  environmental variable which is used to set the path (this is for user
  functions), if that's not found it then falls back on the default path

## Oct  15, 2020 (0.105) 

* Changed default path from x:/anthill to z:/anthill

## March 6, 2020 (0.104)

* Added arcgisversion to configuration file
  if gridio is loaded after anthill it will honor this configuration 
  allowing the arcgis path to be configured for the whole cluster at once

## August 22, 2018 (0.103) 

* Fixed bug introduced in 0.102. 
  
## August 15, 2018 (0.102) 

* Fixed edge condition in kill that caused problems if the 
  first thread to check in on a machine was the only one to be killed on that machine.
* Fixed bug introduced in 0.101. 

## July 11, 2018  (0.101) 

* Updated order of header columns for projects and tasks 
  in settings.R. Prior order didn't match documented order.  This only affects
  files when antinit was used to create new files from scratch.

## February 1, 2018 (0.100) 

* Fixed bug in info() function that caused it to fail 
  when no task has a subtask.  I'm shocked that I hadn't encountered this 
  previously. The problem code was added some time ago.

## January 18, 2018 (0.099)
  * When sanitizing error messages I now replace 
  semicolons with spaces (in addition to tabs and carriage returns which were 
  already being replaced).  Otherwise a semicolon in an error message causes the
  tables to be unreadable.
    
  * Also, added slight pause to simple launch after writing the workspace to 
  hopefully avoid the intermittent error stating the workspace couldn't be 
  loaded because it doesn't exist that is sometimes thrown by the first thread
  working on a project.

## August  21, 2017 (0.098)

*  Edited rerun so that tasks can be rerun multiple times.  
   It now sets tasks with errors to have a control of "run" instead of rerun.
   It considers both "error" and "rerun error" as being valid for rerunning.
   Added "host" as an option for calls to info:  info("host") now returns a handful of columns
   including the host column. For caching efficiency I'm limiting projects to a single host and 
   this makes it easy to see what hosts are in use.  

## August 8, 2017 (0.097)

* `set.onerror()` now sets a projects status to `"pending"` when setting  `onerror`
  to `"continue"`.  
  `set.onerror(onerror = "continue")` is used when a project has thrown
  errors and the user wants to still run the remaining tasks.
  Prior to this update that didn't happen because the project status of 
  `"error"` precluded running the project.

## July 25, 2017 (0.095)

* Added function get.project() to return the name of a project while a task is 
  running.

## July 13, 2017 (0.094) 

* Write task data no longer writes an empty grids.txt file that we don't need or use.

## February 1, 2017 (0.092)

* Added cachetimeout to config.txt.  This specifies how long a copying grid should be waited for 
  in a gridcache before assuming the copy failed. Set in anthill config but used  by gridio when
  caching is on (which requires anthill anyway).

## September 7, 2016 (0.091)

* log.message now looks up the path so it's not necessary to supply it.
	
## August 22, 2016 (0.090)

  * Process tasks now uses the thread ID when getting the initial task data 
  if the thread was passed during a refresh or when switching from APL.
  * Also, manual locks will now have an id of "M" (useful for distinguishing
  stale locks from anthill from user errors.)
  * When splitting up large tasks into multiple tasks launch now shuffles the
  new tasks or not depending on the
   original task. 
   
## July 15, 2016 (0.089)

* Launch will no longer attempt to frisk the cache if caching is off

## May 27, 2016 (0.088)

* Added cachewait to the columns info prints by default.  
* Also added cachewait to settings and launch.project.

## May 23, 2016 (0.087)

* Fixed minor bug in info('stale') that caused a row of NA's to be printed if a project hadn't run at all (no pings yet).
* Added support for cachewait tracking with slight modifications to do.task and update.task.data as well as new functions in cachewait.R.

## April 28, 2016 (0.086)

* Fixed bug in launch.gridservers that caused it to launch servers for all 
  machines.  Also updated code so it adds a title to gridserver windows 
  (if not called from RStudio)
* Added code to info() so that "stale" gives information on stale projects 
  (tailored to blame whoever launched them).
	
## March 22, 2016 (0.084, 0.085)

* Fixed bug in kill.threads that was erroneously changing error and thread counts for non-target projects and tasks.
*   And bug that wasn't properly setting killed project status to "error"

## March 15, 2016 (0.083)

* Debug task updated so that you can use the project index.
* simple.launch now sanitizes project names immediately.  
  This keeps the project directory in sync with the project name. 
* info() truncates the subtask column to 15 characters unless long = T.
* Several other minor bug fixes.

## March 15, 2016 (0.081, 0.082)

* Added kill function to kill projects, tasks, subtasks, or computers. A bunch
  of related private functions as well:  `killthreads()`, `launchmore()`, and 
  `lockpurge()`.
  Also, substantial changes to process tasks and minor changes to other 
  functions to support killing.
* Retire has several new arguments that now allow you to retire the entire 
  cluster (as before), entire computers, a number of threads on a computer, or 
  targeted threads.
* Changed launch.project function so it no longer gives a nonsense warning 
  when launching some projects.

## January, 8 2016 (0.080)

*   Added host column to projects so that projects can be set to run on a specific host or hosts.

## November 20, 2015 (0.079)

* Fixed bug in parse subtask that caused it to behave weirdly if there was 1 subtask and scramble was in effect.
* New function make.range streamlines the process of making a range based subtask argument for simple.launch.

## November 14, 2014 (0.077)

* rerun now defaults to errors.only = TRUE.
* rerun should now reset the error count regardless for all parameterization combinations
  
## August 22, 2014 (0.076)  

* New function update.workspace can be used to add or overwrite objects within 
a projects workspace.

* Fixed bug in debug.task that prevented it from working 
when the error occurred immediately after a wait line.  debug.task also returns 
invisibly the dumped.frames, previously it returned them normally which spammed
the consul.

* Added splitsubtasks argument.  It works basically the same as the old maxsubtasks
but allows APL to have a higher limit (maxsubtasks) that is uses to throw an error
when exceeded while R will split tasks if they are longer than splitsubtasks.

* Lockinfo and lockstatus were updated to be hip to locks that might not end with "/".
If info is called on a project the order in which it prints project info is reversed.  This puts the most important info last where it is most easily viewed on the consul.

## July, 20, 2014 (0.075) 

* start.thread() and antinit() now create and modify anthill/maxthread.txt which stores the id of the last thread launched.  start.thread() uses this to guarantee a unique ID to each thread launched (without recycling). This was added so that killed threads when automatically relaunched have new ID's - eliminating confusion around duplicate ID's and locks in the queue which belong to threads that no longer exist.

## June, 16, 2014 (0.074)

* launch(), when it calls friskcache(), now supplies 0 for the new maxhours 
argument instead of 999 for the old, pw argument. 

## June 5, 2014 (0.073) 

* Edited code in update.task.data so that the message 
  "project finished with errors" is only displayed once.

## May 19, 2014 (0.072)

* launch.project now splits task level maxthreads
  proportionally when splitting up a task with many subtasks.
  Removed package detaching from load.workspace (introduced in 0.069).  You
  cannot safely detach and reattach packages in R.  Instead I may try and 
  refresh the thread when switching projects but have not implemented that yet.

## May 15, 2014 (0.071)

* launch.project now properly updates dependencies while
  splitting up tasks with many subtasks.
* Fixed bug in compact.range that caused it to fail if there was only one
    value.

## May 15, 2014 (0.070) 

* Launch project now doesn't scramble the subtasks prior
  to breaking them up into multiple tasks (when there are more than maxsubtasks).
  This keeps the subtasks field from getting really long.

## May 13, 2014 (0.069)

* Process tasks now saves the global options prior to 
  entering the main task loop and resets the options right after doing each
  task. Someone set options(warn=2) in one project and it bled into others;
  this allows each task to have a clean, default set of options.
  load.workspace now attempts to detach all non-standard and non-anthill 
   packages when switching workspaces.

## May 13, 2014 (0.068)

* Updated rerun to replace the project message with "rerun".  
  That way long error messages don't persist in projects that have no errors.
* Launch project now will automatically split tasks with more than
    maxsubtasks, subtasks into multiple tasks.  
* Default onerror is now "debug" (was "stop").  The behavior is the same except 
    that debug dumps frames to a file which can be inspected later.


## April 17, 2014 (0.067)

* Added taskwait.

## April 15, 2014 (0.066)

* Updating to honor onerror options.

## April 10, 2014 (0.065)

* New config setting maxsubtasks controls how many subtasks
  a task can have.  parse.subtask now throws an error if the subtask produces
  more than this many.


## April 8, 2014 (0.064)

* Updated parse.subtask so that it honors the ! modifier 
  which indicates no scrambling. 

* simple.launch now exports both the global environment and it's parent frame 
  or, if objects argument is used it searches those two environments.  To 
  achieve this it copies objects into a new , local environment from those two
  environments and then saves that environment to the new workspace.  Note: in 
  both environments hidden objects are not included unless specified via 
  objects.


## March 28, 2014 (0.063)

* Updated code in switch system for refreshing in R.
  It now uses Psexec and rflags.  

## March 27, 2014 (0.062)

* Added rflags config variable with default value: 
  `"--sdi --no-restore --max-mem-size=1610612736  --no-save"`
  
* This is now used to set the flags used to launch R.  
  Previously it
  was hard coded in as "--sdi --no-restore".

## March 14, 2014 (0.061)

*   Launch now calls friskcache when appropriate (if the caching is on and no 
  threads are running on the computer)
*   Fixed bug in lockstatus and locknames created when the lockserver started 
  purging lock queues.

## March 12, 2014 (0.059)

*   Added short.name function which returns the shortname of the computer because gridio needs it.

## March 10, 2014 (0.058) 

* Edited read.task.data so that it will hang indefinitely when task data is missing rather than creating empty files.  

## Feb 20, 2014 (0.057)

*  Refresh logging bug fixed.
*  Maintenance threads now produce status.txt if webpath is set in config.txt.

## Feb 12, 2014 (0.056)  

* Calls to cleanup() in process tasks are now explicitly calls to gridio::cleanup() to avoid inadvertently calling cleanup() in package ?spam?. 

## Feb 4, 2014 (0.055) 

* Anthill now refreshes threads if gridio is loaded and gridinitcount() returns more than the value stored in .settings$maxlocalgridinitcount (a new config variable).

## Jan 24, 2014 (0.053, 0.054) 

* Simple launch was listing objects from the global environment but then saving objects from within it's own scope when packaging the user's workspace.  This only mattered if the object existed both in the global environment and in simple.launch's frame.  Now it's both listing and saving from the global environment.  This was most critical with the "path" variable which I tend to use a lot.   The user's path was being overwritten with anthill's causing headaches.
	
## January 21, 2014 (0.052)

* config no longer creates a config file (if it doesn't already exist) instead it attempts to read it repeatedly and then gives up.
* antinit now creates a config file if it doesn't exist.

## Jan 2, 2014 (0.051)  

* Added taskcost column to tasks.txt this allows users to assign a relative cost to each task for more accurate progress reporting.

## Dec 6, 2013 (0.050)

* Another updated locklib.dll  In this one locknames only returns the names of locks that are held or have queues.  New function set.control. 

## Dec 2, 2013 (0.049)

* I now force computer names to be upper case.
* Updated locklib dll.
* Moved dll into inst/src/i386 folder. 
* Added subtask and subtaskarg to simple.launch

## Nov 19, 2013 (0.047) 

* Added maintenance threads (previously only implemented in APL).

## Nov 15, 2013 (0.044) 

*	Added config option scramblesubtask which should be either 0 or 1.  This is used by parse subtask which will scramble the order of it's result if the option is set to 1.  

## Nov 12, 2013 (0.043) 

* Added function findgridserver
* Changed do.task to evaluate the command in the global environment.
Previously the project's workspace was loaded into the global environment
but the command was executed within the do.task function's environment.
Generally this worked but if the project workspace included objects
that were defined differently within the function
then errors would occur when the command found the version within do.task 
instead of the version in the global environment.
* The info function now stores a cached copy of the taskdata.  
This cached copy is used anytime a user specifies a project with a numeric 
or logical index in a user function 
to look up the name of the project from the prior info call.
For instance, the sequence `info()`, `purge.project(3)` will now purge the 
project that was third in the list that the info call produced even if 
someone else has in the interim purged project 1.  


## Sept 24, 2013 (0.042)

* 	Added function readgridservers 

## Sept 17, 2013 (0.041)

*	Cleaned up type on default config comments.
*	Changed function launch.gridservers to reference the file gridservers.txt in the anthill directory.

## September 12, 2013 (0.040)

* new config option uselocktimeout defaults to FALSE if set to TRUE locks can timeout triggering a recovery.  If set to FALSE threads will just keep waiting for the lock. 
* Dropped functions, config options, and project columns related to tracking and launching gridservers and reference grids.  We've always just done this manually and with changes in the gridio library doing it automatically through anthill just seems like a very bad idea.

## July 30, 2013 (0.039) 

* New function compact range (previously part of the LCAD code base) reverses the expand.range function.

## June 28, 2013 (0.038)

Made anthill more robust to transient file system errors.
* New function antinit creates anthill files in an anthill directory by default leaving pre-existing files untouched.  
* read.task.data no longer creates taskdata from scratch if the files aren't on disk.  Instead it loops endlessly until it can read the file - thus the new function antinit is needed to create the files.  

## May 30, 2013 (0.037)

*  simple.launch now puts the project launch directories it creates in a subfolder called "rlaunch" within the anthill directory rather than directly into the anthill directory.  purge.projects now deletes any of these directories that don't have associated projects in the projects file. 
Make project index generates a more informative error when it is called with a project name that doesn't exist.

## May 9, 2013 (0.036)

* info function updated with "what" and "project" arguments consolidated 
into a single argument "x".
* Purge thread now has a force argument allowing threads listed as running
to be purged (useful for crashed threads).
* launch.project now checks for ";" 
 (the comment character we are using in anthill) and tabs and returns;
 and scrubs them from all fields. 

## April 16, 2013 (0.035)

* return.lock and get.lock both attempt to reinitialize the connection with the lock server and try again if the first attempt results in an unknown error.  I'm hoping this will fix a problem in which an error is returned if it's been a long time since the lock server was used.   

## April 8, 2013 (0.034) 

*  Added more user interface functions: simple.launch, launch.test.project, set.priority, and set.maxthreads.

## March 13, 2012 (0.031)

*  Added support for Edi's lockserver.
*  Added support for maxthreads.
*  Added threads column to tasks (number of threads working on the task)

## Sept 25, 2012 (0.029)

*  purge.threads bug fixed.

## August 31, 2012 (0.028)

* cleanup() is now called whenever an R process is quitting (either because of
retire, switch.system, or refresh flags).  This is good practice but we are
also hoping it will allow R to cleanly quit and not leave ghost instances of R
running but not visible o the cluster machines.

## August 15, 2012 (0.027)

*  Make workspace now makes workspaces to launch a specific number of threads.

## August 14, 2012 (0.026)

* Adjusted how sleep interval is randomized.  Now each sleep event has a uniform random jitter equivalent of +/- 1/5 of the sleep interval.  

## July 31, 2012 (0.025)

* More tweaking of lock.dir this time I'm only writing thread info into the last lock file.

## July 19, 2012 (0.024)
*  Modified lock.dir to try and get around occasional collisions. Writing thread info into locks is now done with cat rather than writeLines. 

## July 13, 2012 (0.023)
*  config now works if sleep_in_apl is set to "TRUE" (as opposed to unquoted TRUE)  The quotes are required for apl not to get grumpy.

## June 14, 2012 (0.021)
*  lock.dir now uses file.create to make all the locks and then after all the locks are secured
it writes the thread info and timestamp.  When I switched to creating files with calls to writeLines
collisions resulted and errors were occasionally thrown from within writeLines.  Hopefully this will
alleviate that.

## June 7, 2012   (0.020)

*  lock.dir now rechecks the time immediately before creating each lock file so the timestamp won't be stale.
*  lock.dir is slightly smarter about checking for the computer name (it shouldn't ever be left out now).
*  check.ping is now robust to NA in the timelimit field (NA's shouldn't be there but if present those threads don't time out).  It also is now aware of "maintenance" threads and error logging is more robust in particular if there are missing project directories or tasks with no associated project it will no longer throw an error.

## May 18 (0.017 - 0.019)	

* I now export time functions from the namespace. Attempted a fix at logging bug in checkping that occurred when threads had timed out.
*	Added new configuration parameter sleep_in_apl which defaults to TRUE.  If it is TRUE than R threads switch to APL rather than sleep.
*	start.thread now properly sets the initial value of computers$killed to 0 instead of NA.

## May 8, 2012 

* get.pid now uses the gridio getpid function if gridio is loaded.

## May 3, 2012 (0.014, 0.015) 

* Bug fix get.thread should now work.  Global retire respected.

## May, 1, 2012 (0.013)

* Bug fix.

## April, 30, 2012 (0.012)

*  tm.read.table now has default argument comment=";" so "#" can be inside the table data (brad uses it)"

## April, 24, 2012 (0.011) 

Bug fixes:
* parse.subtask now puts quotes around each element in the vector if it is a character or factor.  This means the command in the subtask will perceive the argument as a character and not as an r object name.  
* info now correctly reads subtask lists.
* New function anthill.setting returns the named setting.


## April 10, 2012 (0.010)

* PID's now stored in thread table
* Launching and switching now relies on psexec but happily doesn't leave a chain of processes.  
* Read task data is more tolerant of variation in the files.  It now allows extra columns and columns to be in any order.
  
## March 27, 2012 (0.009)

* Added get.thread() function

## March 5, 2012 (0.008)

* Added info function.
* Revised and expanded rerun function.

## Feb, 7, 2012 (0.007)

*	Fixed bug in launch.project which didn't release the file lock when  preview was TRUE.  
*	Fixed bug in find.task which was overzealous in reverting waiting tasks to pending. Now it only does so for projects that are pending.
*	Fixed bug in purge.threads.

## Sept 26, 2011 (0.005) 

*	Major update.  Added check.kill, retire, and refresh functions.  Fixed bugs in logic Brad had discovered. Changed launch.project so that it increments a number at the end of the project name if the project already exists rather than reporting an error and substitutes "_" for illegal characters in the project name.

## July 14, 2011, (0.004)

*	Added code to handle the dependencies field.
*	Modified read.task.data and launch project so they will ignore unnecessary columns (previously read.task.data deleted and reordered the columns to match a standard and launch.project expected that standard).
	
