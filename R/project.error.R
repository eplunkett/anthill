project.error <- function(td, project){
  # returns TRUE if the project status should be updated to error
  # which should happen if there are errors and there are no pending tasks
  onerror <- td$projects$onerror[td$projects$project == project]
  tsv <- td$tasks$project == project
  s <- td$tasks$status[tsv]
  rerun.sv <- td$tasks$control[tsv] == "rerun"
  if(onerror %in% "continue"){
    return(all(s %in% c("finished", "rerun finished", "rerun error", "error")) 
           && any(s %in% c("rerun error", "error")) 
           && all(s[rerun.sv] %in% c("rerun error", "rerun finished")) )
  } else { # kill, stop, debug
    error.occured <- any(s[!rerun.sv] %in% "error") || any(s[rerun.sv] %in% "rerun error")
    if(error.occured && onerror == "kill"){
      # Killing would happen here if we ever get it to work
    }
    return(error.occured)
  }
  

}
