toomanygridinits <- function(){
  if( !any( c("package:gridio2", "package:gridio") %in% search() ) ) return(FALSE)
  return(gridio2::gridinitcount() >= .settings$maxlocalgridinit)
}