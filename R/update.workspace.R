if(FALSE){
obj <- c("calc.dev.prob")
project <- "ner-test3_1_sprawl"
project <- "ner-test2_2_sprawl"
update.workspace(obj, project)
}



#' Update the workspace of an anthill project
#' 
#' `update.worksapce()` copies named objects from the users global 
#' environment to an existing project's workspace, overwriting any objects in 
#' the workspace with  the same name. 
#' It allows correcting errors in a launched project and is typically followed
#' by [rerun()] to rerun just the errors. 
#' This can be useful if the errors are edge cases so most subtasks 
#' run but a few do not.
#' 
#' `update.worksapce()` currently doesn't do any locking so best to only
#' call it when the  project isn't running.
#' 
#' @param obj A character scalar or vector containing the names of object(s) to
#' be added to or updated in the project workspace.
#' @param project The name of a project to be updated.
#' @export update.workspace
update.workspace <- function(obj, project){
  # arguments 
  #  object a character vector or scaler of object names
  #  project : the name of a project for which you wish to update the workspace
  #  the object(s) named in the first argument will be added to the workspace of the project
  #   replacing any previous versions
  td <- read.task.data()
  return.lock()
  if(!project %in% td$tasks$project)
    stop("Project '", project, "' is not in the anthill queue.", sep ="")
  wp <- unique(td$tasks$workspace[td$tasks$project == project])
  if(length(wp) > 1 ) stop("Project uses more than one workspace. I'm not sure how to proceed.")
  e <- new.env()
  load(file = wp, envir = e)
  for(i in 1:length(obj))
    assign(x=obj[i], value=get(x = obj[i], envir = .GlobalEnv), envir = e)
  save(list = ls(e, all.names = TRUE), file = wp, envir = e)   
}






