#' Expand a string containing digits ":", and "," into the vector
#' of integers indicated by the string.
#' 
#' This function converts strings like `"1:4, 6, 9, 10:12" `
#' into numeric vectors: `1, 2, 3, 4, 6, 9, 10, 11, 12`.
#' 
#' This is implemented by concatenating "c(", \code{s}, and ")" and then
#' evaluating the result as R code.
#' 
#' @param s a string containing digits separated by commas and colons. Spaces
#' are allowed. Other characters are not.
#' @return A numeric vector.
#' @note This is used to process a "range:" tags in the subtasks column into
#' reps in the sublist.
#' @seealso \code{\link{parse.subtask}} is used to parse the subtask and calls
#' this function. \code{\link{compact.range}} reverses the process.
#' @examples
#' expand.range("1:5,9,13:17")
#' @export
expand.range <- function(s){
	# arguments s: a string specifying a range of number e.g. "1:4, 7, 13:16"
	s <- gsub("[[:space:]]", "", s)
	if(length(grep("[^[:digit:],:]", s))>0) stop("illegal characters in s")
	eval(parse(text=paste("c(", s, ")")))
}
