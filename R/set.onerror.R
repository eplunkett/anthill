#' @rdname set.project.attributes
#' @export
set.onerror <- function(project, onerror, path) {
  valid <- c("continue", "stop", "kill", "debug")
  if (!all(onerror %in% valid)) 
    stop("onerror must be one of the following: ", paste(valid, 
                                                         collapse = ", "))
  config(path)
  path <- .settings$path
  td <- read.task.data()
  a <- tryCatch({
    pi <- make.project.index(project, td)
    if (length(onerror) != length(pi)) 
      onerror <- rep(onerror[1], length.out = length(pi))
    td$projects$onerror[pi] <- onerror
    
    # If reseting onerror to "continue" also reset status to "pending"  this 
    # will cause projects with errors to start running remaining tasks.
    continue.pi <- pi[onerror == "continue"]  # (pi has row indicies)
    td$projects$status[continue.pi] <- "pending"
  }, error = function(e) e)
  if (inherits(a, "error")) {
    return.lock(path)
    stop(a$message)
  }
  else {
    write.task.data(td, path)
  }
}