#' @rdname task-information
#' @return \code{get.taskdetails()} returns a list: 
#' \item{command}{(character) The command that is executed to complete 
#' the task.}
#' \item{workspace}{(character) The path to the workspace that is loaded for
#' the task.} 
#' \item{basepath}{(character) When executing a task r changes the
#' working directory to this path.} 
#' \item{onerror}{ (character) controls what should happen with errors. 
#' See \code{\link{set.onerror}} for possible values.
#' } 
#' \item{directory}{(character) The directory associated with the project.
#' This is where anthill stores temporary files related to the project. It will
#' always end with "/"} 
#' \item{task}{(integer)the task being executed}
#' \item{subtask}{(integer) the subtask being executed}
#' All of the above will be \code{NA} if anthill isn't executing a task.
#' @export
get.taskdetails <- function(){
  # Ethan  March 2022
  # function to return the details of the currently running task
  #  in interactive mode will always return NA
  #  while anthill is executing a task it will return details about that task
  # 
  taskdetails <- .settings$taskdetails
  if(is.null(taskdetails)){
    taskdetails <- list( command=NA, 
                         workspace=NA, 
                         basepath=NA, 
                         onerror=NA,
                         directory=NA,
                         task=NA,
                         subtask=NA)
  } else {
    taskdetails$directory <- gsub("(/|\\\\)*$", "/", taskdetails$directory)
  }
  return(taskdetails)
}