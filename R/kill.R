# 1.	KILL, the user function that requests kills. 
# It figures out the argument (you can kill threads, a project, 
# a task, subtasks, or a computer), finds the threads, sets their controls 
#  (not status, as we'd originally thought) to 'killed,' and reports the 
#  request to the Anthill log.



#' Stop running anthill projects, threads, tasks, subtasks, or computers
#' 
#' `kill()` allows killing (shutting down) anthill threads associated with
#' a project, task, subtask, or computer, without waiting for them to complete.
#' 
#' `kill()` looks up any running threads (nodes) associated with the
#' project, task, subtask, or computer and changes the control to "killed".
#' This will trigger other threads on that computer to kill 
#' the targeted threads within short order (but not instantly.)
#' 
#' [PSTools](https://technet.microsoft.com/en-us/sysinternals/pstools.aspx)
#' must be installed on the host computer for this function to work.
#' 
#' @param what specifies what should be killed. For projects you may use either
#' the project name,its index in the project table, or a logical vector with
#' one item for each row in the project table.  For specifying the subtasks and
#' tasks you must use a character indicating the project name, the task id, and
#' (optionally) subtask id all separated by "." e.g.: testproject.1.3 for the
#' third subtasks of the first task of the project "testproject".  For
#' computers you use the computer's short name (as it is listed in
#' [info] output).
#' @param path This is the path to the anthill cluster.  Only needed if this is
#' the first interactive call to anthill and you aren't using the default path.
#' @return Nothing is returned. It prints out a message indicating what was
#' targeted and how many threads were killed.
#' @seealso [retire()], [purge.project()], [purge.threads()]
#' @export
kill <- function(what, path){
  
  config(path)
  path <- .settings$path
  td <- read.task.data()
  # This try catch is here so that it's harder to bring down the cluster.  
  # It catches errors so that the lock can be released prior to reporting the error
  a <- tryCatch({
    if( is.numeric(what) | is.logical(what)) {
      pi <- make.project.index(what, td)
      what <- as.list(td$projects$project[pi])
      input.level <- rep(1, length(what))
    }  else {  # what is character and contains "." indicates a task or subtask is being killed
      
      what2 <- strsplit(what, "[.]")
      input.level <- sapply(what2, length)
      sv <-  input.level == 1
      input.level[sv][what[sv] %in% td$computers$computer] <- 4  # if the input was a string that matches a computer
      what <- what2
    } 
    # Input level : 1 = project, 2 = task, 3 = subtask, 4 = computer
    zkilled.threads <- numeric(0)
    for(i in 1:length(what)){
      project <- what[[i]][1]
     
      if(input.level[i] == 1){ # project level
        # If entire project is killed also set control of project to "stop" so no new threads are launched
        td$projects$control[td$projects$project == project ] <- "stop"
        sv <- grep(paste("^", project, "[.]",sep="", collapse="|"), td$threads$taskid) 
      }
      if(input.level[i]  == 2){  # task level
        sv <- grep(paste("^", project, "[.]", what[[i]][2],sep="", collapse="|"), td$threads$taskid)
      }
      if(input.level[i] == 3){  # subtask level
        sv <- grep(paste("^", project, "[.]", what[[i]][2], "[.]", what[[i]][3], sep="", collapse="|"), td$threads$taskid)
      }
      if(input.level[i] == 4){  # computer level
        sv <-  td$threads$computer == what[[i]][1]
      }
      td$threads$control[sv] <- "killed"
      killed.threads <- c(killed.threads, td$threads$thread[sv])
    }
    targets <- paste(sapply(what, function(x) paste(x, collapse =".", sep="")), collapse = "\", \"", sep ="")
    msg <- paste0("kill called on \"", targets, "\" (", 
                 length(unique(killed.threads)), " threads)")
    log.message(path=path,  message=msg)
    cat(msg, "\n")
    
  }, error=function(e) e) #  End tryCatch
  if(inherits(a, "error")){
    return.lock(path)  
    stop(a$message)
  } else {
    write.task.data(td, path)  
  }  
    invisible(killed.threads)
}  # end kill function

