#' Get the path to the anthill base directory
#' 
#' If the anthill path has been set with a call to [config()] this
#' will return the path. Otherwise it will return `NULL`. This is used by
#' a many functions in \pkg{gridio} but may be useful in other situations as
#' well.
#' 
#' @return Will return `NULL` if the anthill path has not been set (with
#' [config()]) and the path to the anthill base directory otherwise.
#' The path will never have a trailing delimiter ("\").
#' @seealso
#' * [get.thread()], [get.project()], [get.taskdetails()] for retrieving 
#' information about running tasks. 
#' * [computer.name] for retrieving the name of the current computer.
#' * [short.name()] for the shortened version.
#' @export
get.ant.path <- function(){
  # Function to get anthill path
  # I want to export a public function for this so I don't have to use
  # anthill:::.settings$path in gridio2
  .settings$path
}
