#' Determine the name of the computer
#' 
#' `computer.name()` returns the computer name.  
#' `short.name()` returns the shortened version.
#' 
#' They use shell and a DOS command to extract the computer name, and only
#' works on windows. They will throw an error if [config()] has not been called.
#' 
#' @aliases computer.name short.name
#' @return `computer.name` returns the name of a computer.
#' @seealso \code{\link{process.tasks}} relies on this function.
#' @export
computer.name <- function(){
	# Ethan Plunkett  March 24, 2011
	# Function to get the name of a computer (under windows)
	# Tested under windows XP, Server 2003
	if(.Platform$OS.type != "windows") {
#		stop("computer.name function only works on windows")
		warning("computer.name only works on windows. On all other systems it returns \"testname\".")
		return("testname")
	}
  if(!is.null(.settings$computername) && !is.na(.settings$computername)) return(.settings$computername)
  
	a <- system("ipconfig /all", intern=TRUE)
	a <- a[grep("Host Name", a)]
	a <- gsub(".+:[[:space:]]+|[[:space:]\\]+$", "", a)
	a <- tolower(a)
	.settings$computername <- a
  return(a)
}
