#' Recover from a timeout that occurs while locking a directory
#' 
#' `timeout()` is an internal function called by [lock.dir()] when the lock 
#' files are persistently present.
#' The assumption is that something has crashed before unlocking the
#' directory.  This function logs the timeout to the recovery directory and,
#' importantly, checks the log to make sure no one else has recently performed
#' a recovery. It is not intended for the end user.
#' 
#' @param path the path of the directory where the lock timeout has occurred.
#' @param id the thread id of the the thread performing the lock.
#' @param max.wait the minimum number of seconds that must have passed since
#' the last reset for a new reset to be considered necessary.
#' @param ...  other arguments passed onto [get.lock()].
#' @return `TRUE` if a reset was necessary.  FALSE otherwise.
#' @seealso [lock.dir()]
#' @keywords internal
timeout <- function(path, id, max.wait, ...) {
  cat("\n  Timeout triggered.  Recovering...\n") 
  if(.settings$uselockserver){
    a <- lockstatus()
    if(a$duration < max.wait){
      cat("Lock isn't older than timeout. Lock recovery canceled.\n")
      return(FALSE)
    }
  }
  path <- gsub("/$", "", path) # delete trailing /
  dir <- paste(path, "/recovery",sep="")
  if(!file.exists(dir)) dir.create(dir)
  get.lock(dir, id, max.wait, ...) # lock recovery dir
  fn <- paste(dir, "/recovery.log", sep="")
  if(!file.exists(fn)){
    log <- data.frame(date=NA, computer=NA, id=NA, message=NA, stringsAsFactors=FALSE)
    r <- 1
    #log <- log[-1, ,drop=FALSE]	
  } else {
    log <- tm.read.table(fn)
    names(log) <- tolower(names(log))
    lr <- log$date[nrow(log)] # last recovery
    lr <- as.POSIXct(lr, format=anthill.time.format())
    et <- difftime(Sys.time(), lr, units="secs") 
    if(et < max.wait){
      cat("Recovery was already successful within timeout.\n")
      cat("Last recovery:", log[nrow(log),], "\n")
      return.lock(dir) # unlock recovery directory
      return(FALSE)
    } # end if (timeout)
    log <- rbind(log, NA)
    r <- nrow(log) 
  } # end else (no log file)
  
  log$date[r] <- tm.now()
  log$computer[r] <- computer.name()
  log$id[r] <- id
  log$message[r] <- "Recovered from timeout"
  tm.write.table(log, file=fn)
  return.lock(path) # primary directory (origninal stuck lock)
  return.lock(dir) # recovery directory 
  cat("Recovery successfull.  Lock removed.\n")
  return(TRUE)
} # end function definition
