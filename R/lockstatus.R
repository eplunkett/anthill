
#' @return `lockstatus()` returns the a list of the holder 
#' and duration held in seconds.
#' @rdname lockinfo
#' @export
lockstatus <- function(path,  slash=TRUE){
  if(missing(path)) path <- .settings$path
  if(is.null(.settings$lockinit) || is.na(.settings$lockinit) || !.settings$lockinit)
    stop("Lockserver not initialized or in use.  Call lock config prior to this function and make sure that
          config.txt has uselockserver=1 set.")
  
  
  # extern "C" LOCK_API void R_lock_status(int *ret_v, char **directory, char **lock_holder/* return buffer, must be 64 characters long */, int *lock_duration/* in  seconds */);
  if(slash)
    path <- gsub("/*$", "/", path) # enforce trailing "/"
  
  r <- .C("R_lock_status", integer(1), as.character(path), character(1),  integer(1),  PACKAGE="lock_lib")
  
  if(r[[1]] %in% c(8, 5, 4)) return(list(duration=0, holder=NA))  # special case for error that is thrown when there is no queue
  .parse.error(r[[1]], "lock")
  
  return(list(duration=r[[4]], holder=r[[3]]))
  
}
