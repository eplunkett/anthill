#' Check for kill directives in running cluster tasks.
#' 
#' This function checks to see if a command has been issued to kill the running
#' process and ends it if it has.
#' 
#' Salting your code with calls to this function allows the task to be killed
#' prior to completion.
#' 
#' @return It returns nothing.  If a kill instruction is detected it reports an
#' error "Task Killed" which will stop the process and be trapped by the
#' Anthill code.
#' @keywords internal
anthillcheck <- function(){
  if(exists(".settings") && exists("path", envir=.settings)){
  	kd <- paste(.settings$path, "/kill/", sep="")
  	if(file.exists(paste(kd, .settings$project, sep=""))  | file.exists(paste(kd, .settings$project, ".", .settings$task)))
	stop("Task Killed")  	
  }  
}


