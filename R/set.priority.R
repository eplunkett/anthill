#' @rdname set.project.attributes
#' @export
set.priority <- function(project, priority, path){
    
  config(path)
  path <- .settings$path
  td <- read.task.data()
  # This try catch is here so that it's harder to bring down the cluster.  
  # It catches errors so that the lock can be released prior to reporting the error
  a <- tryCatch({
    
    # Generate a project index which contains the index (numerical) of the project(s) in the projects table
    pi <- make.project.index(project, td)
    
    # expand priority out to match the length of the selected projects
    if(length(priority) != length(pi)) priority <- rep(priority[1], length.out=length(pi))  
    if(!is.numeric(priority)) stop("Priority must be numeric.")
    
    td$projects$priority[pi] <- priority
  
    
   # Either write out the task data or release the lock and report errors.
  }, error=function(e) e) #  End tryCatch
  if(inherits(a, "error")){
    return.lock(path)  
    stop(a$message)
  } else {
    write.task.data(td, path)	
  }
  
}


