#' Switch task processing from R to APL.
#' 
#' This internal function launches an APL process to process tasks and 
#' then exits R. It is called within [process.tasks()] when 
#' the next task to be processed is an APL task.
#' 
#' @param path the path of the task manager control directory.
#' @param thread the id of the thread this is passed to APL so it will have the
#' same thread id.
#' @param system The system to launch into.(Either R or APL. By default this is
#' APL but you can launch back into R which is done when a refresh is
#' requested.
#' @return The function returns nothing but does have the side effects of
#' launching APL (or a new instance of R) and quitting R.
#' @seealso [launch()], [process.tasks()]
#' @keywords internal
switch.system <- function(path, thread, system="APL"){
  # This function causes r to launch APL (or R) into its taskmanager 
  # workspace and then quit itself.
  path <- gsub("/$", "", path)

  if (system %in% c("R", "r")) {
    if (!file.exists(.settings$rworkspace))
      make.workspace(path) 

    sc <- paste("psexec -i -d ",  .settings$rpath, " ", 
                .settings$rworkspace, " ",  .settings$rflags, 
                " --args thread=", thread, sep="") 
    sc <- gsub("/", "\\\\", sc)
    
    cat(paste("refreshing with shell command '", sc, "'", sep=""), "\n")
    shell(sc, wait=FALSE)
  } else { # APL
  	print("Switching to APL")
    shell(paste(.settings$aplpath, " ", .settings$aplworkspace, " user=", 
                thread + 1, sep = ""), wait = FALSE)		
  }
  Sys.sleep(10)
  q(save = 'no')		
}	
