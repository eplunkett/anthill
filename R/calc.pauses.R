#' Calculate a sequence of pauses unique to a given id.
#' 
#' This function is used by [lock.dir] under file based locking 
#' to determine a unique sequence of pauses for every thread in the cluster.
#' This is used to resolve collisions where the file system allows two processes
#' to make the same file at once.  
# ' File based locking is only used if 
#' `uselockserver = 0` in `config.txt`, otherwise the now standard 
#' lockserver based locking is used.
#' 
#' @param id The id of the thread for which we want to calculate pauses.
#' @param n The total number of threads supported.
#' @param p The number of pauses in the sequence.
#' @param s The length (in seconds) of spacing increment among pauses.
#' @return A numeric vector indicating the length of a series of pauses.
#' @seealso \code{\link{lock.dir}} locks a directory and depends on this
#' function.
#' @keywords internal
calc.pauses <- function(id, n, p=4, s=.1){
# This function returns the series of pauses  to be used by the thread with the given id
#  the sequence is guarenteed to be unqiue for each ID up to n.
# n = number of threads
# p = number of pauses between keys = (keys-1)
# s = is the spacing in the pauses
#	Note id=0 means random pauses.
	if(id==0){
		pauses <- stats::runif(p, 0, 3*s)
	} else {
		id <- id-1 # assuming id's start with 1 we still want to start with zero.
		nl <- ceiling(n^(1/p)) # number at each level
		pauses <- numeric(p)
		for(i in 1:p) pauses[i] <- id %/% nl^(p-i) %% nl
		pauses <- pauses*s
	}
	return(pauses)
}
