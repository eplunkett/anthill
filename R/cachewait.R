


#' Track cachewait
#' 
#' "cachewait" is time spent waiting on gridio2 raster file caches. 
#'  `start.cache.timer()` and `stop.cache.timer()` are used by gridio2 to 
#'  keep track of time spent using the grid cache and so will remain public.  
#'  `clear.cachewait()` and `get.cachewait()` are used internally by anthill
#'  and may not be exported in the future.
#' 
#' Every time `stop.cache.timer()` is called the elapsed time since the last
#' call `to start.cache.timer()` is added to a running tally of cachewait time.
#' `clear.cachewait()` resets that running tally to zero while 
#' `get.cachewait()` returns the tally.
#' 
#' @aliases start.cache.timer stop.cache.timer clear.cachewait get.cachewait
#' @return `get.cachewait()` returns the cumulative cachewait in seconds
#' since the 1st call to `clear.cachewait()`. The other functions don't
#' return anything.
#' @name cache-timing
#' @rdname cachewait
#' @export start.cache.timer
start.cache.timer <- function(){
  .settings$cachetime <- Sys.time()
}

#' @rdname cachewait
#' @export
stop.cache.timer <- function()
  .settings$cachewait <- .settings$cachewait + 
  as.numeric(difftime(Sys.time(),  .settings$cachetime, units = "secs"))

#' @rdname cachewait
#' @export
clear.cachewait <- function()
  .settings$cachewait <- 0

#' @rdname cachewait
#' @export
get.cachewait <- function()
    return(.settings$cachewait)
  