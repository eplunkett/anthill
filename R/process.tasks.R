#' Process cluster tasks on a thread running in the cluster.
#' 
#' `process.tasks()` is run by threads in the cluster. It checks the thread 
#' into the cluster and then cycles through tasks until
#' either there is nothing to do or the next thing to do is an APL task.  In
#' the first case it waits and periodically checks back in.  In the second case
#' it checks out and then calls [switch.system()] which launches APL
#' and then quits R.
#' 
#' [launch()] launches R with the anthill  workspace made by 
#' [make.workspace()]which contains a function `.First()` which in turn
#' calls `process.tasks()`.
#' 
#' @param path The path of the main directory which contains the cluster
#' control files.
#' @return The function returns nothing.
#' @seealso \code{\link{launch}}
#' @keywords internal
process.tasks <- function(path){  
  #------------------------------------------------------------------------------------------------#
  #  process.tasks    Ethan Plunkett                                                    2011
  # 
  #   This is the base function called by nodes on the anthill cluster while they are working. 
  #------------------------------------------------------------------------------------------------#
  if(.settings$verbosity > 1)
    cat("process.tasks called.\n")

  config(path) # set package scoped settings based on config file (or to defaults in the config 
  path <- .settings$path 
  
  threadlimit <- .settings$threadlimit
  
  # Set computer name
  computer <- .settings$computer
  
  # Get thread number (from command line argument)
  ca <- commandArgs(trailingOnly = TRUE)
  ca <- grep("thread=", ca, value=TRUE, ignore.case=TRUE)
  if(length(ca)==1){ # if there was a thread command line argument
    ca <- gsub("thread=", "", ca, ignore.case=TRUE)
    thread <- as.numeric(ca)
  } else { # or thread is 0 indicating no thread assigned yet
    thread <- 0
  }
  rm(ca)

  # Read the task data
  td <- read.task.data(path, thread)

  if(.settings$verbosity > 1)
    cat("process.tasks : task data is read with thread = ", thread, "\n")
  
  
  # Log the thread in.
  result <- start.thread(td=td, thread=thread, computer=computer, path=path,
                         threadlimit=threadlimit)
  td <- result$td
  thread <- result$thread
  rm(result)
  
  # Save  thread, and sleep.interval to the .settings environment 
  .settings$thread <- thread
 
  td.current <- TRUE # flag that indicates that the task data in memory (td) is current (and the directory is locked by this thread)
  slept <- FALSE # flag indicates that the thread slept on the prior iteration through the loop
  
  a <- .settings$recoverservers
  if(!is.null(a) && !is.na(a) && a !=0 ){
    .settings$servercheck <- Sys.time() + .settings$recoverservers*60  
  } else {
    .settings$servercheck <- NA
  }
  
  op <- options()  # save options
  
  while(TRUE){  			# main loop		
    notask <- FALSE  	# if TRUE the thread shouldn't do a task (it will either switch, retire, or sleep) if FALSE there's a task to do.
    switch <- FALSE 	# if TRUE we should switch control over to APL
    retire <- FALSE		# if TRUE then the thread should retire.
    refresh <- FALSE    # if TRUE then the thread should refresh (relaunch itself)
    # There is a fifth state "sleep" that is inferred from all the others:
    #   If there's nothing to do sleep
  
    # If the task data isn't current
    if(!td.current) {
      td <- read.task.data(path, thread)
      td.current <- TRUE   # flag indicating that td in memory is up to date
    } 
    # Update sleep interval
    sleep.interval <- max(.settings$sleep*(nrow(td$threads)), .settings$sleep, na.rm=TRUE) 
    
    # Kill any threads that have been flagged for killing (on this machine)
    A <- kill.threads(td = td)
    td <- A$td
    retire <- retire | A$retire
    
    # Check for system wide file based retire command
    if(file.exists(paste(path, "/retire", sep=""))) retire <- TRUE
    
    # Check for system wide file based refresh command
    if(file.exists(paste(path, "/refresh", sep=""))){
      td$threads$control[!td$threads$control %in% c("retire", "killed")] <- "refresh"
      file.remove(paste(path, "/refresh", sep="")) 
      log.message(path=path, message="Refresh initiated (R)", thread=thread)
      cat("Refresh initiated\n")
    }
    
    # Launch threads to replace killed threads
    td <- launchmore(td = td, retire = retire)
    
    
    # Check to see if it's OK for this thread and computer to run 
    result <- check.thread(computer=computer, thread=thread, td=td, path=path)
    notask <- notask | result$notask
    retire <- retire | result$retire
    refresh <- refresh | result$refresh
    rm(result)    	
    
    # Check for threads that have exceeded their timelimit and flag them and their tasks as errors
    # td <- check.ping(td=td, path=path)
    
    if(retire  | refresh) notask <- TRUE  # needed for system wide commands
    
    
    
    # Find next task
    if(!notask){
      task.id <- find.task(td, path)
      switch <- task.id$apl
      if(!task.id$success) notask <- TRUE # no next task for R
      td <- task.id$td
      task.id <- task.id[ !names(task.id) %in% c("success", "td","apl" )]  # drop extra stuff from task.id
      # Save the project, task, and subtask - required for anthill.check 
      .settings$project <- task.id$project
      .settings$task <- task.id$task
      .settings$substask <- task.id$subtask
    }
    
    
    # Check for sleep_in_apl config setting and update flags as appropriate
    if(isTRUE(.settings$sleep_in_apl)  & notask & !switch & !retire){
      switch <- TRUE
      cat("Sleeping while sleep_in_apl is TRUE.\n")
    }
    
    # Check if R is capable of launching threads and update flags as appropriate
    # if R cannot launch threads then if it's not doing something or sleeping
    # it retires - APL maintenance thread will then relaunch missing threads
    # see  tests/testthat/test-launching.R for more details on the launching
    # issues that this hack circumvents
    if(!.settings$r_can_launch && notask && (switch || refresh)) {
      retire <- TRUE
      switch <- FALSE
      refresh <- FALSE
    }
    
    # Update task data to account for whatever we are about to do: do task, switch, sleep, or retire
    if(!notask){ # if there's a task to do now
      task <- get.task(computer=computer, thread=thread, list=task.id, td=td, path=path)
      td <- task$td
      task$td <- NULL
    } else if(switch){
      ti <- which(td$threads$thread %in% thread)[1]
      td$threads$status[ti] <- "switching to apl"
      td$threads$taskid[ti] <- NA
      td$threads$timelimit[ti] <- max(1900, 5*sleep.interval) # 250 sec = 5 min
      ci <- which(td$computers$computer %in% computer)[1]
    } else if(retire){
      ti <- td$threads$thread %in% thread
      td$threads <- td$threads[!ti, , drop=FALSE]  # drop this thread from threads
      ci <- td$computers$computer %in% computer
      tc <- count.threads(td, computer=computer)
      td$computers$threadsrunning[ci] <- tc$threadsrunning
    } else if(refresh){
      ti <- which(td$threads$thread %in% thread)[1]
      td$threads$control[ti] <- "run"
      td$threads$status[ti] <- "refreshing"
      td$threads$taskid[ti] <- NA
      td$threads$timelimit[ti] <- max(1900, 5*sleep.interval) # 250 sec = 5 min
      if(!any(td$threads$control %in% "refresh")){
        log.message(path=path, message="Refresh completed (R)", thread=thread)  # if this is the last one
        print("Refresh complete.")
      } 
    } else { # sleep
      
      # update sleep interval (in case number of threads have changed)
      sleep.interval <- max(.settings$sleep*(nrow(td$threads)), .settings$sleep, na.rm=TRUE)  
      if(.settings$maintenance) sleep.interval <- min(sleep.interval, 60)  # so they are available to kill other thread
      
      
      ti <- which(td$threads$thread %in% thread)[1]
      if(td$taskwait > 30 ) slept <- FALSE # so message get's printed again if a waiting for lock message was printed
      if(td$threads$status[ti] != "sleeping"){  # Only bother to make changes if it's not already sleeping
        td$threads$timelimit[ti] <- max(1900, 5*sleep.interval) # 250 sec = 5 min
        td$threads$status[ti] <- "sleeping"
        if(!td$threads$taskid[ti] %in% "maintenance" ) td$threads$taskid[ti] <- NA
      } # end if it was not already flagged as sleeping.
    } 
    
    # Set thread ping
    ti <- which(td$threads$thread == thread)[1]
    td$threads$ping[ti] <- tm.now()
    
    # Write out the task data 
    write.task.data(td, path)
    td.current <- FALSE
    
    # Do something (switch, sleep, or the task)
    if(!notask){  # if there is a task to do now
      cat("Starting", paste(task.id, collapse="."), "\n")
      slept <- FALSE 
      result <- do.task(task=task)
      options(op)  # reset options (in case the task changed them)
      td <- read.task.data(path, thread)
      td.current <- TRUE
      td <- update.task.data(result=result, td=td, task.id=task.id, 
                             computer=computer, thread=thread, path=path)
    } else if(switch){
      cat("Switching to APL.\n") 
      if("gridio2" %in% .packages()) gridio2::cleanup()  
      switch.system(path, thread)
    } else if(retire){
      cat("Retiring\n")
      Sys.sleep("5")
      if("gridio2" %in% .packages()) gridio2::cleanup()  
      q(save="no")		
    } else if(refresh){
      cat("refreshing R\n") 
      if("gridio2" %in% .packages()) gridio2::cleanup()  
      switch.system(path, thread, system="R")
    } else { # nothing to do so sleep
      st <- sleep.interval + stats::runif(1, 0-sleep.interval*.2, sleep.interval*.2) # random jitter to desyncronize
      if(!slept) cat("Sleeping ", round(st, 1), " sec.\n") # Happens on the first sleep loop only
      slept <- TRUE
      load.workspace("") # sleep triggers a clearing of the (project) workspace 
      
      # If it's a maintenance thread and webpath is set then output status.txt
      wp <- .settings$webpath
      if(!is.null(wp) && !is.na(wp) && wp != ""){ # if web status reporting
        dir <- gsub("[/\\\\]$", "", wp)
        sp <- paste(dir, "/status.txt", sep="")
        ti <- which(td$threads$thread %in% thread)[1]
        if(td$threads$taskid[ti] %in% "maintenance" &&  file.exists(dir) ){
          antstatus(td=td, file=sp)
        }
      }
      rm(wp)
      
      Sys.sleep(st) 
      rm(st)
    }   
    
    # If it's a maintenance thread 
    if(isTRUE(.settings$maintenance)){
      if(!is.na(.settings$servercheck) && Sys.time() > .settings$servercheck){
        relaunch.gridservers()
        .settings$servercheck <- Sys.time() + .settings$recoverservers*60
      }
    }
    
    
  } # end  main loop
} # End Function Definition
