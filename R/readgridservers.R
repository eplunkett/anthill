#' read gridservers.txt 
#' 
#' This function returns a table with the contents of `gridservers.txt` from
#' within the anthill directory. 
#' 
#' @param path (optional) defines the path to the anthill directory.  Defaults
#' to the last path used in an anthill function or, failing that, to a global
#' default that is updated to work on the landscape ecology lab's cluster
#' machines.
#' @param active.only if TRUE (the default) drop rows for gridservers that
#' aren't listed as active.  Otherwise keep all rows.
#' @return A data.frame containing the contents of `gridservers.txt` 
#' (possibly without inactive entries).
#' @seealso [launch.gridservers()]
#' @export
readgridservers <- function(path, active.only=TRUE){
  config(path)
  path <- .settings$path
  path <- gsub("/$", "", path)
  gridservers <- tm.read.table(paste(path, "/gridservers.txt", sep=""))
  gridservers$active <- as.logical(gridservers$active)
  if(active.only) gridservers <- gridservers[gridservers$active, ]
  return(gridservers)
}
