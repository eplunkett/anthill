#' Create a range based subtask argument
#' 
#' `make.range()` creates a text string suitable for supplying as the `subtask`
#' argument to [simple.launch()]. For example `"range!:1:5, 7"`.
#' It can be supplied with either the end point of a range in which case it
#' will assume the range includes all integers from 1 to the specified end; or
#' for irregular ranges it can be supplied with a numeric vector of values in
#' the range.
#' 
#' This is a convenience wrapper to [compact.range()] which is used to convert
#' numbers to text.
#' 
#' @param end Use this argument to specify the end point of a range from `1:end`.
#' @param values Use this argument to specify a non-continuous set of values via
#' a numeric vector containing all the values in the set.
#' @param scramble If `FALSE` then `!` will be inserted into the range string to
#' indicate that the tasks should be executed in the order specified and not
#' scrambled.
#' @return A string suitable for use as the `subtask` argument to 
#' [simple.launch()]
#' @seealso [compact.range] is an older function that generated the
#' text after the first `":"` [parse.subtask()] is used internally to
#' reverse the process and extract a numeric vector from the string.
#' @examples
#' make.range(5, scramble = FALSE)  
#' make.range(values = c(1:5, 7, 8, 9, 11, 15))
#' @export
make.range <- function(end, values, scramble = TRUE){
  stopifnot(is.logical(scramble))
  if(missing(end) & missing(values))
    stop("either values or end must be supplied")
  if(!missing(end) &  !missing(values))
    warning("both values and end arguments were supplied. ignoring values")
  
  if(!missing(end)){
    values <- seq_len(end)
  } 
  res <- paste0("range", 
                ifelse(scramble[1], "", "!"), ":", compact.range(x = values))
  return(res)
}

