#' @rdname set.project.attributes
#' @export
set.host <- function(project, host, path){
  
  if(!is.character(host))
    stop("host should be a character")
  host <- gsub("[[:space:],]+", " ", host)  # remove commas and tabs from host
  
  
  config(path)
  path <- .settings$path
  td <- read.task.data()
  # This try catch is here so that it's harder to bring down the cluster.  
  # It catches errors so that the lock can be released prior to reporting the error
  a <- tryCatch({
    
    # Generate a project index which contains the index (numerical) of the project(s) in the projects table
    pi <- make.project.index(project, td)
    
    # expand priority out to match the length of the selected projects
    if(length(host) != length(pi)) host <- rep(host[1], length.out=length(pi))  
    
    td$projects$host[pi] <- host
    
    
    # Either write out the task data or release the lock and report errors.
  }, error=function(e) e) #  End tryCatch
  if(inherits(a, "error")){
    return.lock(path)  
    stop(a$message)
  } else {
    write.task.data(td, path)  
  }
  
}
