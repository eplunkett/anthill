
#' Get time format string used by anthill
#' 
#' `anthill.time.format()` returns the time format string used by `anthill`.
#' 
#' @return `anthill.time.format()` returns a string designating a date and time 
#' format as described in [strptime()].
#' @seealso [strptime()], [tm.now()]
#' @export
anthill.time.format <- function()
  return("%d %b %Y, %I:%M:%S %p")


#' Format elapsed time
#' 
#' Convert numeric  hours, minutes, or seconds into the elapsed time
#' format used by anthill. Use only one of the arguments in a single call.
#' 
#' @param seconds total number of elapsed seconds. If no argument name is
#' supplied it will assume seconds.
#' @param minutes total number of elapsed minutes.
#' @param hours total number of elapsed hours
#' @return A string that indicates elapsed time in the format
#' `"hours:minutes:seconds"`.
#' @seealso [convert.to.seconds()] to convert back to numeric
#' seconds, [sumelapsedtime()] to sum a vector of elapsed times in the
#' output format.
#' @examples
#' formatelapsedtime(seconds = 5462)
#' @export
formatelapsedtime <- function(seconds = minutes*60.0, minutes = hours*60.0,
                              hours){
	# 
	# Reformats time to hours:minutes:seconds
	# Can take seconds, minutes, or hours as an argument but only give 
  # it one argument
	seconds <- round(seconds)
	time <- paste(seconds %/% 3600, ":", 
	              format((seconds %% 3600) %/% 60, width=2, mode="character"), 
	              ":", format(seconds %% 60, width=2, mode="character"), sep="")
	time <- gsub(" ", "0", time)
	time
}


#' Sum multiple elapsed times
#' 
#' `sumelapsedtime()` sums elapsed times stored in the format 
#' `"hours:minutes:seconds"`, while ignoring `NA` values.
#' 
#' @param x a vector of elapsed time e.g. : `"00:05:01", "00:01:31"`
#' @return The sum of the elapsed times in the format `"hours:minutes:seconds"`
#' @seealso [formatelapsedtime()], [convert.to.seconds()]
#' @examples
#' #sumelapsedtime(c("01:00:01","00:00:59", "00:05:05"))
#' @export
sumelapsedtime <- function(x){
# Function to sum a vector of elapsed times in the format:
#hh:mm:ss	
	
	x <- grep("[[:digit:]]*:[[:digit:]]*:[[:digit:]]", x, value=TRUE)
	
		s <- as.numeric(sapply(x, convert.to.seconds))
	return(formatelapsedtime(seconds=sum(s)))
}


#' Convert anthill elapsed time to seconds
#' 
#' `convert.to.seconds()` converts elapsed time from the form 
#' `"hours:minutes:seconds"` to numeric seconds. It is not vectorized.
#' 
#' @param t a character string in the format of `"hours:minutes:seconds"` e.g.
#' `"00:5:01"` for five minutes and one second.
#' @return the integer number of seconds.
#' @examples
#' # convert.to.seconds("00:05:01")
#' @export
convert.to.seconds <- function(t){
		t <- strsplit(t, "[[:space:]]*:[[:space:]]*")[[1]]
		if(length(t) != 3) stop("malformed elapsed time")
		sec <- as.numeric(t[1])*3600 + as.numeric(t[2])*60 + as.numeric(t[3])
		return(sec)
}


#' Returns the current time
#' 
#' Returns a string for the current date and time the anthill format.
#' 
#' @return a string designating the date and time in the [anthill.time.format()]
#' @seealso \code{\link{sumelapsedtime}}, \code{\link{formatelapsedtime}}
#' @keywords internal
tm.now <- function(){ # return the current time in the task manager time format
	format(Sys.time(), format=anthill.time.format())
}
