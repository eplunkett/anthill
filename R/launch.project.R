if(FALSE){
  library(anthill)
  config()
  name="test"
  ntasks=4
  wait=3
  nsubtasks=205
  task.maxthreads=50
  maxthreads=100
  
  commands <- rep(paste("Sys.sleep(", wait, ")", sep=""), ntasks)

  tasklist <- data.frame(command=commands, subtask=paste("range:1:", nsubtasks, sep=""),
    maxthreads=task.maxthreads)
  
  tasklist <- data.frame(command = "go()")
  tasklist$dependency <- NA
  if(nrow(tasklist) > 1) 
    tasklist$dependency[2:nrow(tasklist)] <- 1:(nrow(tasklist) - 1)
  path <- .settings$path
  directory=paste(path, "/", name, sep="")
  
  priority <- 5
  comment <- NA
  preview <- FALSE
  maxthreads <- 0
  onerror = "stop"
  maxsubtasks = 100
  owner <- "Ethan"
  
  info()
  
  launch.project(name=name,
                 tasklist=tasklist,
                 owner=owner, 
                 maxsubtasks=100
                 )
  info(1, long=T)
  purge.project(1)
  
}




#' Launch an anthill project
#' 
#' `launch()` handles launching projects to run on an anthill cluster.
#' It has largely been replaced by [simple.launch()] which automates more of 
#' the process at the cost of a little bit less control.
#' 
#' The main reasons for using `launch.project` rather than
#' [simple.launch] are:
#' 1. Legacy code. 
#' 2. If you want to have one workspace file that is used for multiple projects
#' `simple.launch` creates a workspace at launch from the user's
#' environment - although you can specify which objects to include if you want.
#' 3. If you have really complex dependencies, although the "wait" based
#' dependencies that is invoked simply by having some command lines contain a
#' "wait" are available in [simple.launch] and is easier to use.  
#' 4. If you really want to set task costs for more accurate percent 
#' complete values.
#' 
#' There are hidden defaults buried in the function.  If the `tasklist` is a
#' vector or if it is a `matrix` with less than the full suite of columns than
#' the missing columns will be filled in with these values: `subtask=NA`,
#' `subtaskarg=NA`, `system="R"`, `workspace=NA`, `timelimit=86400`, and
#' `comments=NA`.
#' 
#' To use subtasks you should use the `subtaskarg` and `subtask` columns of
#' `tasklist`.
#
#' Here's a simple example:  
#' `tasklist <- data.frame(command = "fun()", subtaskarg = "i",  
#'                         subtask = "range:1:10")`  
#'                      
#' In this example the function `fun()` will be called ten times with an
#' argument `i` set to 1 through 10. 
#' They may be run concurrently and the order will be randomized.
#' `tasklist <- data.frame(command = "fun()", subtaskarg = "i",  
#'                          subtask = make.range(10,scramble = FALSE))`  
#' Is equivalent but the order won't be randomized.
#' 
#' @param name the name of the new project.
#' @param tasklist either a character vector containing a series of commands
#' (one per task) or a dataframe with a subset of the following columns:
#' \describe{
#' \item{`command`}{(required) a series of commands stored in
#' text format. May contain "wait" items in which case commands after each
#' "wait" will not run until all prior commands have successfully completed.}
#' \item{`subtask`}{`NA` (the default) or a line to generate
#' subtasks (see [parse.subtask()]) for how this should be specified
#' and [make.range()] to create appropriate strings.}
#' \item{`subtaskarg`}{`NA` (the default) or an argument name to be
#' used to pass the subtask to the command} 
#' \item{`system`)}{the system in which the command will be executed 
#' (either `"R"` or `"APL"`), and almost always `"R"` (the default) when
#' launching from R} 
#' \item{`workspace`}{`NA` or the path to either an `.Rdata` or `.R` file 
#' which should be loaded or executed prior to running the command} 
#' \item{`timelimit`}{timeout (in seconds) beyond which 
#' the task will be considered hung; `comments` comments to
#' be associated with each task. Defaults to 172,800.}
#' \item{`comments`}{for human reading only, rarely used, `NA` by default.}
#' \item{`maxthreads`}{can be used to set the task-specific maximum
#' number of threads, which can be useful to limit read-write intensive tasks
#' but not processor intensive tasks. An alternative is to set the
#' project-level maxthreads by using the maxthreads argument. It defaults to 0
#' which indicates no limit.}
#' \item{`maxpernode`)}{can be used to set thE task specific maximum number 
#' of threads per computer.  Limiting the number
#' of threads on each computer may be useful to distribute memory intensive
#' processes among machines while limiting the number on each machine.}
#' \item{`comments`}{for human reading only, rarely used, NA by default.}
#' \item{`taskcost`}{the relative cost of the tasks in the project.  Will
#' be used to weight the tasks when calculating the percent complete.  It
#' defaults to 1 for each task which makes all tasks equal cost.}
#' \item{`dependency`}{Can be used to indicate which prior tasks each
#' task depends on. Each task will not start until all the tasks it depends on
#' are complete. Should characters that can be processed with [expand.range()]} 
#' 
#' } % end tasklist describe 
#' 
#' If `tasklist` is a data frame the only required column is `command`.
#' @param owner The project owner. It may be omitted if [set.owner()]
#' has been called.
#' @param basepath The working directory will be changed to this path prior to
#' any project related code being run.  If left blank than the working
#' directory will not be modified by the anthill software; Vut can be modified
#' by code in your projects functions.
#' @param directory The directory for project-specific logs and subtask files.
#' It defaults to a subfolder with the same name as the project within the the
#' rlaunch directory within anthill's base path. Project specific subtask lists
#' and potentially logs will be stored here.
#' @param path Base path for anthill files. Defaults to the path appropriate
#' for the landscape ecology cluster. Can be set universally with
#' [config()].
#' @param priority project priority (higher numbers = higher priority)
#' @param comment optional text comment to be associated with the project
#' @param preview If `TRUE` dataframes are returned for viewing and are
#' not appended to the taskmanager files on disk. Note the task dataframe that
#' is returned if this option is TRUE will only ave the columns specified in
#' the arguments but the file on disk will have all the standard task columns.
#' @param maxthreads The maximum number of threads that should be assigned to
#' the project. 0 indicates no limit. This can be used to crudely load balance
#' the cluster.  If a project is very heavy on grid input and output it is
#' recommended to set this so that the project doesn't occupy many threads
#' which all then wait for the gridserver.
#' @param host Indicates the host or hosts on which the project should run. The
#' default value of "" indicates any host. Otherwise it should be a space
#' delimited list of computer short names (see [short.name]).
#' @param onerror Controls what happens when a task in the project produces an
#' error.  Options are: \describe{
#' 
#' \item{`"stop"`}{No new threads will be launched, running threads will 
#' not be interrupted.} 
#' \item{`"kill"`}{ Not implemented. Currently the same as
#' `"stop"` but is intended to allow running threads to be interrupted
#' whenever any thread produces an error.} 
#' \item{`"continue"`}{Remaining tasks continue to run normally 
#' as long as they are not dependent on any task with
#' an error} 
#' \item{`"debug"`}{Uses [dump.frames()] to dump the call
#' stack at the point of the error into a file specific to the project and
#' task.  Running tasks finish but no new tasks are launched.} 
#' }
#' @param maxsubtasks Any tasks that has more than this many subtasks will be
#' split up into multiple tasks each of which has no more than this many
#' subtasks. This only applies to subtasks that begin with "range:".  Use
#' \code{NA} for no splitting.  The default is to use the \code{maxsubtask}
#' value from \code{config.txt}.
#' @return The function returns nothing unless \code{preview} is \code{TRUE} in
#' which case it returns preview dataframes.  If preview is \code{FALSE} it
#' will launch the project by adding the data to the taskmanager files.
#' @author Ethan Plunkett
#' @seealso \code{\link{simple.launch}} Easier project launching recommended in
#' most cases.\cr \code{\link{purge.project}}\cr \code{\link{launch}} Launches
#' anthill threads to make them available for running projects.\cr
#' \code{\link{info}} for tracking projects.\cr
#' @examples
#' \dontrun{
#'  launch.project(name="test", tasklist=rep("Sys.sleep(2)", 20), preview=TRUE)
#'  launch.project(name="test", tasklist=data.frame(command="Sys.sleep(2)", 
#'                 subtask="range:1:20"), preview=TRUE)
#'                 
#'   config()
#' name="test"
#' ntasks=4
#' wait=3
#' nsubtasks=205
#' task.maxthreads=50
#' maxthreads=100
#' 
#' commands <- rep(paste("Sys.sleep(", wait, ")", sep=""), ntasks)
#'
#' tasklist <- data.frame(command=commands, subtask=paste("range:1:", nsubtasks, sep=""),
#'                       maxthreads=task.maxthreads)
#' 
#' tasklist <- data.frame(command = "go()")
#' tasklist$dependency <- NA
#' if(nrow(tasklist) > 1) 
#'   tasklist$dependency[2:nrow(tasklist)] <- 1:(nrow(tasklist) - 1)
#' path <- .settings$path
#' directory=paste(path, "/", name, sep="")
#' 
#' priority <- 5
#' comment <- NA
#' preview <- FALSE
#' maxthreads <- 0
#' onerror = "stop"
#' maxsubtasks = 100
#' owner <- "Ethan"
#' 
#' info()
#' 
#' launch.project(name=name,
#'                tasklist=tasklist,
#'                owner=owner, 
#'                maxsubtasks=100)
#' info(1, long=T)
#' purge.project(1)
#' }
#' @export
launch.project <- function(name, tasklist, owner, basepath = "", 
                           directory = paste0(path, "/rlaunch/", name), 
                           path, 
                           priority=5, comment=NA, preview=FALSE, maxthreads=0,
                           host = "",
                           onerror="debug", maxsubtasks){
  # Generate a new project. Gets a lock, adds a project to the project file 
  #  and task file, and releases the lock.
  # If preview is TRUE the function returns the data it would have added to 
  # the task and project lists.  Some task columns may be missing from the
  # preview; they will be populated with NA values in the final list.
  
  config(path)	 
  path <- .settings$path
  if(missing(owner)  && !is.null(.settings$owner) && !is.na(.settings$owner))
    owner <- .settings$owner
  
  if(missing(owner) || !is.character(owner) || is.na(owner) || length(owner) != 1 ) 
    stop("Missing or malformed owner. Owner should be a character scalar. What's your name?")
  owner <- gsub("[\n\r\t]", "", owner)  # get rid of problem characters
  owner <- gsub(";#", "-", owner)  # get rid of problem characters
  
  
  if(!is.character(host) & length(host == 1))
    stop("host should be a character of length 1")
  host <- gsub("[[:space:],]+", " ", host)  # remove commas and tabs from host
  
  
  # Strip illegal characters from project names
  name <- .clean.project.name(name)

  
  td <- read.task.data(path)
  
  # Note defaults for projects are largely set in the arguments to the function but some columns
  #  in projects are set in the code below - these are generally columns the user would have no 
  #  reason to set like pctgridwait or errors which always start at 0
  
  
  # This try catch is here so that it's harder to bring down the cluster.  
  # It catches errors so that the lock can be released prior to reporting the error
  a <- tryCatch({
    
    # Special case if the task list is a vector assume that it's a vector of commands
    if(is.vector(tasklist)) tasklist <- data.frame(command=tasklist)
    
    # Formatting and validation 
    if(is.matrix(tasklist)) tasklist <- as.data.frame(tasklist)
    if(!"command" %in% names(tasklist))
      stop("if tasklist is a dataframe it must contain a column labled \"command\".")
    
    
    # Check for duplicated project name
    if(name %in% td$projects$project){
      n <- 2
      name2 <- name
      while(name2 %in% td$projects$project){
        name2 <- paste(name, n, sep="")	
        n <- n+1
      }	
      warning("A project named ", name, " already exists.  Using \"", name2,"\" instead.", sep="")
      name <- name2
    } # end if name already in use
    
    
    # Set the default value for optional fields for the tasks table 
    optional.cols <- list(subtask=NA, subtaskarg=NA, system="R", workspace=NA, 
                          timelimit=172800, comments=NA, project=name, 
                          control="run", status="pending", 
                          task=1:nrow(tasklist), maxthreads=0, maxpernode=0, threads=0, 
                          taskcost=1) 
    # Note: NA values will be written out as ""
    
    
    if("dependency" %in% names(tasklist)) for(i in 1:nrow(tasklist)){
      a <- tryCatch(expand.range(tasklist$dependency[i]), error = function(e) e)
      if(inherits(a, "error")) 
        stop("Dependency value ", tasklist$dependency[i], 
             " can't be processed with expand range.  Message:", a$message)
    }
    
    
    # Create the project directory if it doesn't exist
    if(!file.exists(gsub("/$", "", directory))) 
      dir.create(directory, recursive=TRUE)
    
    # Convert factor columns to character
    for(i in 1:ncol(tasklist)) if(is.factor(tasklist[,i])) 
      tasklist[,i] <- as.character(tasklist[,i])
    
    
    for(i in setdiff(names(optional.cols), names(tasklist))) 
      tasklist[[i]] <- optional.cols[[i]] 
    
    
    # Split up tasks with many subtasks
    #   If maxsubtasks is defined then any task which has more than the maximum
    #   subtasks will be broken up into multiple tasks each below the maximum
    #   After splitting the tasks up we also update the dependencies and 
    #    the taskcost to reflect the meaning conveyed in the original values
    if(missing(maxsubtasks))
      maxsubtasks <- .settings$splitsubtasks
    
    if(!is.na(maxsubtasks)){
      maxsubtasks <- round(maxsubtasks)
      if(maxsubtasks < 100)  
        stop("maxsubtasks should be at least 100.  Smaller values are silly.")
      tasklist$splitme <- FALSE
      sv <- grepl("range", tasklist$subtask)
      f <- function(x) length(parse.subtask(x, check=FALSE)) > maxsubtasks
      if(any(sv))
        tasklist$splitme[sv] <- sapply(tasklist$subtask[sv], FUN=f)
      if(any(tasklist$splitme)){
        while(any(tasklist$splitme)){
          row <- match(TRUE, tasklist$splitme)
          subtasks <- parse.subtask(subtask=tasklist$subtask[row], 
                                    check=FALSE,
                                    scramble=FALSE)
          n <- ceiling(length(subtasks) / maxsubtasks )
          newsubtasks <- character(n)
          nsubtasks <- numeric(n)
          shuff <- !grepl("!", tasklist$subtask[row], fixed = TRUE)
          for(i in 1:n){
            start = (i -1)* maxsubtasks + 1
            end = min(start + maxsubtasks - 1, length(subtasks))
            
            newsubtasks[i] <- paste0("range", ifelse(shuff, "", "!"), ":",  compact.range(x=subtasks[start:end]))
            nsubtasks[i] <- end - start + 1
          }
          tasklist$splitme[row] <- FALSE
          this.task <- tasklist[rep(row, n), ] 
          this.task$subtask <- newsubtasks
          prop <-  nsubtasks /sum(nsubtasks)
          this.task$taskcost <- round(this.task$taskcost[1] * prop, 3)
          this.task$maxthreads <- ceiling(this.task$maxthreads[1] * prop)
          tasklist <- rbind(tasklist[(1:nrow(tasklist)) < row, ], 
                            this.task,
                            tasklist[(1:nrow(tasklist)) > row, ])
          
        }  # end loop through rows that need splitting
        rownames(tasklist) <-  1:nrow(tasklist)
        
        # Update dependencies (After splitting)
        if("dependency" %in% names(tasklist)){
          dep <- which(!is.na(tasklist$dependency) & tasklist$dependency != "")
          if(length(dep)> 0) for(i in dep){  # for each row with dependencies
            od <- expand.range(tasklist$dependency[i])  # old dependencies
            nd <- (1:nrow(tasklist))[tasklist$task %in% od]
            tasklist$dependency[i] <- compact.range(nd)
          }# end if maxsubtasks defined  
          
        } # end if dependency column in taskslist
        tasklist$task=1:nrow(tasklist)
      } # End if any split
      tasklist$splitme <- NULL
      
    } # end if maxsubtasks set
    
    # Create a new dataframe out of the project
    # Note if you change the order of the columns here you should also 
    # change it in settings.R
    project <- data.frame(project = name, 
                          control = "run",
                          status = "pending", 
                          pctcomplete = 0, 
                          threads = 0, #5
                          errors = 0, 
                          priority = priority, 
                          maxthreads = maxthreads, 
                          host = host,
                          launch = tm.now(), 
                          onerror = onerror,
                          lastactivity = tm.now(), 
                          tasks = nrow(tasklist), 
                          completed = 0, 
                          processortime = 0, 
                          pctgridwait = 0, 
                          pctcachewait = 0,
                          pcttaskwait = 0, 
                          pctlockwait = 0,
                          message = NA, 
                          owner = owner, 
                          basepath = basepath,  
                          directory = directory,
                          comment = comment, 
                          stringsAsFactors = FALSE)
    
    # Verify consistency with settings$file.info
    stopifnot(all(names(project) == .settings$file.info$project.header))
    
    # Deletes comment characters, tabs and carriage returns from dataframes
    # to sanitize the tabular data
    scrub <- function(x){
      x <- gsub("[;]", "",x) 
      x <-  gsub("[[:blank:]\n\r\t]+", " ", x)
    }
    for(i in 1:ncol(tasklist)){
      if(is.character(tasklist[[i]]))
        tasklist[[i]] <- scrub(tasklist[[i]])
    }
    for(i in 1:ncol(project)){
      if(is.character(project[[i]]))
        project[[i]] <- scrub(project[[i]])
    }
    
    
    if(preview){
      return.lock(path)	
      return(list(tasks=tasklist, project=project))
    }
    
    if(all(names(project) %in% names(td$projects) )){  
      pr <- nrow(td$projects)+1
      td$projects[pr, ] <- NA # add new row
      td$projects[pr, names(project)] <- project
    } else {
      stop("All the new project columns must be in td$projects.  Unexpexpected columns:", 
           paste(setdiff(names(project), names(td$projects)), collapse=", "))
    }
    
    # Add the new tasks
    
    # Check for invalid column names in tasklist
    if(!all(names(tasklist) %in% names(td$tasks))) 
      stop(paste(setdiff(names(tasklist), names(td$tasks)), collapse=", "), 
           " are not valid tasklist column names." )
    
    # First add the right number of new rows (all with NA values) to td$tasks
    first.new.row <- nrow(td$tasks) +1
    td$tasks[first.new.row, ] <- NA 
    td$tasks <- td$tasks[c(1:first.new.row, rep(first.new.row, nrow(tasklist)-1)), ]
    rownames(td$tasks) <- 1:nrow(td$tasks)
    # Next dump the tasklist into those rows (in the appropriate columns)
    nri <- first.new.row : nrow(td$tasks) # new row index
    td$tasks[nri, names(tasklist)] <- tasklist
    
    # Finally either write out the task data or release the lock and report errors.
  }, error=function(e) e) #  End tryCatch
  
  if(inherits(a, "error")){
    return.lock(path)	
    stop(a$message)
  } else {
    write.task.data(td, path)	
  }
}
