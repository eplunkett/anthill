
# anthill

<!-- badges: start -->
<!-- badges: end -->



Anthill is a file based task management system for a windows based cluster.
It tracks tasks, projects, threads, and computers each with their own file.

It was built in conjunction  with 
[gridio](https://bitbucket.org/eplunkett/gridio) 
and the two are somewhat integrated. 

Ethan Plunkett  
University of Massachusetts  
Landscape Ecology Lab   
plunkett@umass.edu   

## Installation

You can install the development version of anthill like so:

``` r
devtools::install_bitbucket("eplunkett/anthill")

```



