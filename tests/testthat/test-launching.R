
# Note before the switch to R 4.3 R was able to launch an independent R or APL
# Process that would persist after the launching instance closed.
# With R 4.3 that is no longer TRUE.
# The tests here document different approaches I attempted to launch independent
# processes.  They all failed.  And all are currently skipped when running
# tests for the package.


test_that("psexec can launch indpendent process", {
  
  skip()
  
  dir <- withr::local_tempdir()
  dir.create(dir, showWarnings = FALSE)
  
  ws1 <- file.path(dir, "ws1.Rdata")
  ws2 <- file.path(dir, "ws2.Rdata")
  complete.file <- file.path(dir, "completed.txt")
  
  
  
  rpath <- "C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe"
  
  # Note: internal escaping of " in windows command is done with """  
  # See:  
  shell_cmds <- paste0(
    "psexec -i -d ", '"', rpath, '" ',
    c(ws1, ws2), " --sdi --no-restore --no-save --args")

  
  shell_cmds <- gsub("/", "\\\\", shell_cmds)                 
  sc1 <- shell_cmds[1]
  cat("Launching command 1:\n", sc1, "\n")
  
  sc2 <- shell_cmds[2]
  cat("Launching command 2:\n", sc2, "\n")
  
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    cat("Loaded ws1\nlaunching ws2\n")
    system(command = sc2, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
           show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
           wait = FALSE, timeout = 0)
    cat("Quiting in 14 seconds.\n")
    Sys.sleep(14)
    q("no")
  }
  save(.First, complete.file, sc2, ws2, file=ws1)
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    n <-30
    each <- 2
    
    cat("Loaded ws2\nStalling for ", n * each, " seconds.\n", sep = "")
    for(i in seq_len(n)){
      Sys.sleep(each)
      cat(i, " of ", n, ": ", format(Sys.time()), "\n", sep = "")
    }
    cat("Writing complete file\n")
    writeLines("test", con = complete.file)
    cat("Exiting\n")
    Sys.sleep(1)
    q("no")
  }
  save(.First, complete.file, file=ws2)
  
  system(command = sc1, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
         show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
         wait = FALSE, timeout = 0)
  
  
  Sys.sleep(20)
  
  
  expect_true(file.exists(complete.file))
  
})





test_that("Anthill can spawn independent RScript processes", {
  # test if I can spawn independent processes
  testthat::skip()
  
  
  dir <- withr::local_tempdir()
  dir.create(dir, showWarnings = FALSE)
  
  s1 <- file.path(dir, "s1.R")
  s2 <- file.path(dir, "s2.R")
  complete.file <- file.path(dir, "completed.txt")
  
  for(f in c("s1", "s2", "complete.file")){
    assign(f, gsub("\\\\", "/", get(f)))  
  }
  
  
  
  
    
a <- paste0('cat("launching ws2\n")
     system2("rscript", args = c("', s2 , '", "--vanilla"), 
             wait = FALSE, minimize = FALSE, invisible = FALSE)
     cat("Quiting in 10 seconds.\n")
     Sys.sleep(10)
     q("no")')
  
  writeLines(a, con = s1)
  
b <- paste0(
    'n <- 8
    each <- 2
    
    cat("Stalling for ", n * each, " seconds.\n", sep = "")
    for(i in seq_len(n)){
      Sys.sleep(each)
      cat(i, " of ", n, ": ", format(Sys.time()), "\n", sep = "")
    }
    cat("Writing complete file\n")
    writeLines("test", con = "', complete.file ,'")
    cat("Exiting\n")
    Sys.sleep(1)
    q("no")')
  
  writeLines(b, con = s2)

  system2(command = "rscript", args = c(s1, "--vanilla"),
          wait = FALSE, minimized = FALSE, invisible = FALSE)

  # This launches two windows but the second terminates when the first closes
  
  
  
  Sys.sleep(20)
  expect_true(file.exists(complete.file))
  
})




test_that("Anthill can spawn independent Rgui processes", {
  # test if I can spawn independent processes
  testthat::skip()
  
  dir <- withr::local_tempdir()
  dir.create(dir, showWarnings = FALSE)
  
  ws1 <- file.path(dir, "ws1.Rdata")
  ws2 <- file.path(dir, "ws2.Rdata")
  complete.file <- file.path(dir, "completed.txt")
  
  rpath <- "\"C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe\""
  
  launch.prefix <- "start  "
  launch.suffix <- "--sdi --no-restore --no-save"
  
  
  # shell command one
  sc1 <- paste0(launch.prefix, " ", rpath, " \"", ws1, "\" ", launch.suffix) # New code launches with psexec
  sc1 <- gsub("/", "\\\\", sc1)
  cat("Launching command 1:\n", sc1, "\n")
  
  # shell command two
  sc2 <- paste0(launch.prefix, " ", rpath, " \"", ws2, "\" ", launch.suffix) # New code launches with psexec
  sc2 <- gsub("/", "\\\\", sc2)
  cat("Launching command 2:\n", sc2, "\n")
  
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    cat("Loaded ws1\nlaunching ws2\n")
    shell(sc2, wait = FALSE, intern = FALSE, shell = Sys.getenv("COMSPEC"))
    cat("Quiting in 10 seconds.\n")
    Sys.sleep(10)
    q("no")
  }
  save(.First, complete.file, sc2, ws2, file=ws1)
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    n <- 8
    each <- 2
    
    cat("Loaded ws2\nStalling for ", n * each, " seconds.\n", sep = "")
    for(i in seq_len(n)){
      Sys.sleep(each)
      cat(i, " of ", n, ": ", format(Sys.time()), "\n", sep = "")
    }
    cat("Writing complete file\n")
    writeLines("test", con = complete.file)
    cat("Exiting\n")
    Sys.sleep(1)
    q("no")
  }
  save(.First, complete.file, file=ws2)
  
  shell(cmd = sc1, wait = FALSE, intern = FALSE, shell = Sys.getenv("COMSPEC"))
  
  Sys.sleep(20)
  expect_true(file.exists(complete.file))
})

if(FALSE){

shell.exec("X:\\Anthill\\r_ant.bat")
}

test_that("Powershell can launch indpendent process", {
  
  skip()
  
  dir <- withr::local_tempdir()
  dir.create(dir, showWarnings = FALSE)
  
  ws1 <- file.path(dir, "ws1.Rdata")
  ws2 <- file.path(dir, "ws2.Rdata")
  complete.file <- file.path(dir, "completed.txt")
  
  
  
  rpath <- "\"C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe\""
  
  # Note: internal escaping of " in windows command is done with """  
  # See:  
  shell_cmds <- paste0("powershell  -command ",
                        '"Start-Process -FilePath """C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe\""" ',
                         '-ArgumentList """', c(ws1, ws2),'""""')
                          
  shell_cmds <- gsub("/", "\\\\", shell_cmds)                 
  sc1 <- shell_cmds[1]
  cat("Launching command 1:\n", sc1, "\n")
  
  sc2 <- shell_cmds[2]
  cat("Launching command 2:\n", sc2, "\n")
  
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    cat("Loaded ws1\nlaunching ws2\n")
    system(command = sc2, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
           show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
           wait = FALSE, timeout = 0)
    cat("Quiting in 10 seconds.\n")
    Sys.sleep(14)
    q("no")
  }
  save(.First, complete.file, sc2, ws2, file=ws1)
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    n <-30
    each <- 2
    
    cat("Loaded ws2\nStalling for ", n * each, " seconds.\n", sep = "")
    for(i in seq_len(n)){
      Sys.sleep(each)
      cat(i, " of ", n, ": ", format(Sys.time()), "\n", sep = "")
    }
    cat("Writing complete file\n")
    writeLines("test", con = complete.file)
    cat("Exiting\n")
    Sys.sleep(1)
    q("no")
  }
  save(.First, complete.file, file=ws2)
  
  system(command = sc1, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
         show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
         wait = FALSE, timeout = 0)
  
  
  Sys.sleep(20)
  
  
  expect_true(file.exists(complete.file))
  
  
  
  
  
})




test_that("Powershell can launch indpendent process", {
  
  skip()
  
  dir <- withr::local_tempdir()
  dir.create(dir, showWarnings = FALSE)
  
  ws1 <- file.path(dir, "ws1.Rdata")
  ws2 <- file.path(dir, "ws2.Rdata")
  complete.file <- file.path(dir, "completed.txt")
  
  
  
  rpath <- "\"C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe\""
  
  # Note: internal escaping of " in windows command is done with """  
  # See:  
  shell_cmds <- paste0("powershell  -command ",
                        '"Start-Process -FilePath """C:/Program Files/R/R-4.3.2/bin/x64/Rgui.exe\""" ',
                         '-ArgumentList """', c(ws1, ws2),'""""')
                          
  shell_cmds <- gsub("/", "\\\\", shell_cmds)                 
  sc1 <- shell_cmds[1]
  cat("Launching command 1:\n", sc1, "\n")
  
  sc2 <- shell_cmds[2]
  cat("Launching command 2:\n", sc2, "\n")
  
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    cat("Loaded ws1\nlaunching ws2\n")
    system(command = sc2, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
           show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
           wait = FALSE, timeout = 0)
    cat("Quiting in 10 seconds.\n")
    Sys.sleep(14)
    q("no")
  }
  save(.First, complete.file, sc2, ws2, file=ws1)
  
  .First <- function(){
    .First.sys() # loads some standard stuff
    n <-30
    each <- 2
    
    cat("Loaded ws2\nStalling for ", n * each, " seconds.\n", sep = "")
    for(i in seq_len(n)){
      Sys.sleep(each)
      cat(i, " of ", n, ": ", format(Sys.time()), "\n", sep = "")
    }
    cat("Writing complete file\n")
    writeLines("test", con = complete.file)
    cat("Exiting\n")
    Sys.sleep(1)
    q("no")
  }
  save(.First, complete.file, file=ws2)
  
  system(command = sc1, intern = FALSE, ignore.stdout = TRUE, ignore.stderr = TRUE,
         show.output.on.console = FALSE, minimized = FALSE, invisible = FALSE, 
         wait = FALSE, timeout = 0)
  
  
  Sys.sleep(20)
  
  
  expect_true(file.exists(complete.file))
  
  
  
  
  
})





